package com.example.myvideogame.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.myvideogame.R
import com.example.myvideogame.databinding.FragmentConclusionBinding
import com.example.myvideogame.databinding.FragmentHelpBinding

class HelpFragment : Fragment() {

    lateinit var binding: FragmentHelpBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHelpBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.gotit.setOnClickListener {
            findNavController().navigate(R.id.action_helpFragment_to_menuFragment)
        }
    }
}