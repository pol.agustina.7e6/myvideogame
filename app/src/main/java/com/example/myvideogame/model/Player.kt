package com.example.myvideogame.model

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.RectF
import com.example.myvideogame.R

class Player(context: Context,screenX: Int,screenY: Int) {
    var bitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.player)
    val width = screenX / 15f
    val height = screenY / 15f
    var positionX = screenX/2
    var positionY = screenY*2/3
    var hitbox = RectF()

    init{
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)
    }
}
